@echo off

rem Install-Module ps2exe before
rem or download from here: https://www.powershellgallery.com/packages/ps2exe/

chcp 65001>nul
CD /D "%~dp0"
set ps1=designer.ps1
set exe=designer.exe
set ico=designer.ico
set title="PowerShell FormDesigner"
set product="PowerShell FormDesigner"
set c=https://bitbucket.org/mozers
set tm=mozers™
set comp=mozers™

for /f "tokens=3 delims=='" %%a in ('find "$Version = " %ps1%') do set ver=%%a
echo %p% version %ver%

powershell -Command Invoke-ps2exe -inputFile '%ps1%' -outputFile '%exe%' -noConsole -verbose -iconFile '%ico%' -title '%title%' -product '%product%' -copyright '%c%' -trademark '%tm%' -company '%comp%' -version '%ver%'

pause
