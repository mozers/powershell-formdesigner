Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing
[Windows.Forms.Application]::EnableVisualStyles()
#
#Form0
#
$Form0 = New-Object System.Windows.Forms.Form
$Form0.BackColor = "Gray"
$Form0.FormBorderStyle = "Fixed3D"
$Form0.MaximizeBox = $false
$Form0.Text = "Small Calculator"
$Form0.ClientSize = New-Object System.Drawing.Size(402, 73)
#
#GroupBox0
#
$GroupBox0 = New-Object System.Windows.Forms.GroupBox
$GroupBox0.Text = ""
$GroupBox0.Size = New-Object System.Drawing.Size(386, 63)
$GroupBox0.Location = New-Object System.Drawing.Point(8, 2)
$Form0.Controls.Add($GroupBox0)
#
#TextBox0
#
$TextBox0 = New-Object System.Windows.Forms.TextBox
$TextBox0.Font = New-Object System.Drawing.Font("Arial", 24, [System.Drawing.FontStyle]::Bold)
$TextBox0.TextAlign = "Center"
$TextBox0.Size = New-Object System.Drawing.Size(72, 44)
$TextBox0.Location = New-Object System.Drawing.Point(8, 12)
$GroupBox0.Controls.Add($TextBox0)
#
#ComboBox0
#
$ComboBox0 = New-Object System.Windows.Forms.ComboBox
$ComboBox0.Font = New-Object System.Drawing.Font("Courier New", 24, [System.Drawing.FontStyle]::Bold)
$ComboBox0.Size = New-Object System.Drawing.Size(40, 44)
$ComboBox0.Location = New-Object System.Drawing.Point(88, 12)
$ComboBox0.Items.AddRange(("+", "-", "/", "*"))
$GroupBox0.Controls.Add($ComboBox0)
#
#TextBox1
#
$TextBox1 = New-Object System.Windows.Forms.TextBox
$TextBox1.Font = New-Object System.Drawing.Font("Arial", 24, [System.Drawing.FontStyle]::Bold)
$TextBox1.TextAlign = "Center"
$TextBox1.Size = New-Object System.Drawing.Size(72, 44)
$TextBox1.Location = New-Object System.Drawing.Point(136, 12)
$GroupBox0.Controls.Add($TextBox1)
#
#Button0
#
$Button0 = New-Object System.Windows.Forms.Button
$Button0.Font = New-Object System.Drawing.Font("Arial", 24, [System.Drawing.FontStyle]::Bold)
$Button0.Text = "="
$Button0.Add_Click({$TextBox2.Text = Invoke-Expression -Command ($TextBox0.Text + $ComboBox0.Text + $TextBox1.Text)})
$Button0.Size = New-Object System.Drawing.Size(44, 46)
$Button0.Location = New-Object System.Drawing.Point(216, 11)
$GroupBox0.Controls.Add($Button0)
#
#TextBox2
#
$TextBox2 = New-Object System.Windows.Forms.TextBox
$TextBox2.Font = New-Object System.Drawing.Font("Arial", 24, [System.Drawing.FontStyle]::Bold)
$TextBox2.Text = ""
$TextBox2.TextAlign = "Center"
$TextBox2.Size = New-Object System.Drawing.Size(110, 44)
$TextBox2.Location = New-Object System.Drawing.Point(268, 12)
$GroupBox0.Controls.Add($TextBox2)

[void]$Form0.ShowDialog()

